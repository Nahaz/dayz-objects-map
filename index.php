<?php
// Fetch map data & settings
require("inc/mapdata.php");
require("inc/settings.php");
require("inc/functions.php");

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Refresh" content="60">
    <title>DayZ Entities Map</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
   	<script src="js/bootstrap.min.js"></script>
  	<script src="js/jquery-1.8.2.min.js"></script>
  	<script src="js/jquery-ui-1.8.23.min.js"></script>
  	<script src="js/tooltipsy.min.js"></script>
 </head>
  <body>
    <div id="maparea">
    	<?php 
    		displayData($atv_result, "atv");
    		displayData($heli_result, "heli");
    		displayData($bike_result, "bike");
    		displayData($boat_result, "boat");
    		displayData($bus_result, "bus");
    		displayData($car_result, "car");
    		displayData($tent_result, "tent");
    		displayData($lorry_result, "lorry");
    		displayData($truck_result, "truck");
    		displayData($uaz_result, "uaz");
    		displayData($tractor_result, "tractor");
    	?>
    </div>
    <script src="js/jquery.js">
    </script>
  </body>
</html>