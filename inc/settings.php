<?php
//Database server details
$db_server  	= "host";
$db_name 		= "database";
$db_user 		= "user";
$db_password	= "password";

// Map Calculationvariables, used with function displayData();
// Image size in pixels
$imageX = 1000;
$imageY = 1000;
// Probably won't need to change these, unless Lingor or other maps are different size
// Game Map Size
$mapX = 15360;
$mapY = 15360;
// Scale Game->Image
$divX = $mapX/$imageX;
$divY = $mapY/$imageY;

?>