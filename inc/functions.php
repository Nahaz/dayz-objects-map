<?php
require_once("settings.php");
// Function to display objects on map. Scale and Map vars set to default Chernarus with a 1000px map.
function displayData($result, $type, $scaleX=15.36, $scaleY=15.36, $mapY=15360) {
	foreach ($result as $object => $value) {
    			$worldspace = $value[1];
    			$replace = array("[", "]]");
    			$ws_replaced = str_replace($replace, "", $worldspace);
    			$coords = explode(",", $ws_replaced);
    			$xt = $coords[1];
    			$yt = $coords[2];
    			$x = $xt/$scaleX;
    			$y = ($mapY-$yt)/$scaleY;
    			$xg = $xt/100;
    			$yg = ($mapY-$yt)/100;
    			echo '<div id="obj-vehicle" style="left:'.$x.'px;top:'.$y.'px;">'.
    				 '	<img class="hastip" src="img/'.$type.'.png" />'.
    				 '	<div id="obj-text">'.
    				 '	Name:'.$value[0].
    				 '	<br />X='.$xg.
    				 '	<br />Y='.$yg.
    				 '	</div>'.
    				 '</div>';
    		}
}
?>