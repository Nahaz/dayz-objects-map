<?php
require_once("settings.php");
// Define db connection
$db = new PDO('mysql:host='.$db_server.';dbname='.$db_name.'', $db_user, $db_password);
// Comment the following line to disable error output
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

// Prepared query to pull classname and location for all saved objects (vehicles, tents etc. Wires excluded)
//$obj_query = $db->prepare('SELECT Classname, Worldspace FROM object_data WHERE Classname != "Wire_cat1"');
$atv_query	 	 = $db->prepare('SELECT Classname, Worldspace FROM object_data WHERE Classname LIKE "ATV%"');
$heli_query	 	 = $db->prepare('SELECT Classname, Worldspace FROM object_data WHERE Classname LIKE "UH1H_DZ"');
$bike_query	 	 = $db->prepare('SELECT Classname, Worldspace FROM object_data WHERE Classname IN ("Old_bike_TK_INS_EP1", "Old_bike_TK_CIV_EP1", "TT650_Ins", "TT650_Civ")');
$boat_query	 	 = $db->prepare('SELECT Classname, Worldspace FROM object_data WHERE Classname IN ("Smallboat_1", "Smallboat_2","PBX")');
$lorry_query 	 = $db->prepare('SELECT Classname, Worldspace FROM object_data WHERE Classname IN ("Ural_TK_CIV_EP1","V3S_Civ")');
$bus_query		 = $db->prepare('SELECT Classname, Worldspace FROM object_data WHERE Classname LIKE "Ikarus"');
$car_query		 = $db->prepare('SELECT Classname, Worldspace FROM object_data WHERE Classname IN ("Lada2", "car_hatchback","Volha_2_TK_CIV_EP1", "S1203_TK_CIV_EP1")');
$tent_query		 = $db->prepare('SELECT Classname, Worldspace FROM object_data WHERE Classname LIKE "TentStorage"');
$tractor_query	 = $db->prepare('SELECT Classname, Worldspace FROM object_data WHERE Classname LIKE "tractor"');
$truck_query	 = $db->prepare('SELECT Classname, Worldspace FROM object_data WHERE Classname LIKE "hilux1_civil_3_open"');
$uaz_query		 = $db->prepare('SELECT Classname, Worldspace FROM object_data WHERE Classname LIKE "UAZ_CDF"');

// Run Query
//$obj_query->execute();
$atv_query->execute();
$heli_query->execute();
$bike_query->execute();
$boat_query->execute();
$lorry_query->execute();
$bus_query->execute();
$car_query->execute();
$tent_query->execute();
$tractor_query->execute();
$truck_query->execute();
$uaz_query->execute();

// Fetch data from query
//$obj_result = $obj_query->fetchAll();
$atv_result = $atv_query->fetchAll();
$heli_result = $heli_query->fetchAll();
$bike_result = $bike_query->fetchAll();
$boat_result = $boat_query->fetchAll();
$lorry_result = $lorry_query->fetchAll();
$bus_result = $bus_query->fetchAll();
$car_result = $car_query->fetchAll();
$tent_result = $tent_query->fetchAll();
$tractor_result = $tractor_query->fetchAll();
$truck_result = $truck_query->fetchAll();
$uaz_result = $uaz_query->fetchAll();
?>